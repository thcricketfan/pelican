![Build Status](https://gitlab.com/pages/pelican/badges/master/build.svg)

----

Example [Pelican] website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io.

-----

## Building locally

To work locally with this project, there are a few options. But let's keep it
simple:

1. Fork, clone or download this project
1. [Install] pelican
1. Generate the website: `make html`
1. Preview your project: `make serve`
1. Add content

Read more at Pelican's [documentation].

## GitLab User or Group Page

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

## Forked projects

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

Enjoy!

[pelican]: http://blog.getpelican.com/
[install]: http://docs.getpelican.com/en/3.6.3/install.html
[documentation]: http://docs.getpelican.com/
